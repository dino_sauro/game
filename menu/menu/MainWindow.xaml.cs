﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Media.Imaging;
using System.Threading;

namespace menu
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow
    {
        readonly System.Media.SoundPlayer _music = new System.Media.SoundPlayer();
        bool _isPlay = true;
        public MainWindow()
        {
           
           _music.SoundLocation = "music.wav";
           
 
           InitializeComponent();
           Back.Visibility = Visibility.Hidden;
            Thread.Sleep(100);
           _music.Play();
           

        }

        private void start_Click_1(object sender, RoutedEventArgs e)
        {
            if (_isPlay)
            {
                _music.Stop();
                _isPlay = false;
            }
            else
            {
                _music.Play();
                _isPlay = true;
            }
        }

        private void quit_Click_1(object sender, RoutedEventArgs e)
        {
                Application.Current.Shutdown();
        }

        private void tutorial_Click_1(object sender, RoutedEventArgs e)
        {

            Play.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Quit.Visibility = Visibility.Hidden;
            Back.Visibility = Visibility.Visible;
            var tut = new BitmapImage();

            tut.BeginInit();

            tut.UriSource = new Uri(System.IO.Path.GetFullPath("..//..//..//..//menu//menu//tutImage.jpg"));

            tut.EndInit();

            Ground.ImageSource = tut;
        }

        private void back_Click_1(object sender, RoutedEventArgs e)
        {
            Play.Visibility = Visibility.Visible;
            Tutorial.Visibility = Visibility.Visible;
            Quit.Visibility = Visibility.Visible;
            Back.Visibility = Visibility.Hidden;
            Story.Visibility=Visibility.Visible;

            var tut = new BitmapImage();

            tut.BeginInit();

            tut.UriSource = new Uri(System.IO.Path.GetFullPath("..//..//..//..//menu//menu//backGround.jpg"));

            tut.EndInit();

            Ground.ImageSource = tut;

        }

        private void play_Click_1(object sender, RoutedEventArgs e)
        {
            Process.Start("play.exe");
            Close();
        }

        private void Story_OnClick(object sender, RoutedEventArgs e)
        {
            Play.Visibility = Visibility.Hidden;
            Tutorial.Visibility = Visibility.Hidden;
            Quit.Visibility = Visibility.Hidden;
            Back.Visibility = Visibility.Visible;
            Story.Visibility=Visibility.Hidden;
            var story = new BitmapImage();

            story.BeginInit();

            story.UriSource = new Uri(System.IO.Path.GetFullPath("..//..//..//..//menu//menu//storyImage.jpg"));

            story.EndInit();

            Ground.ImageSource = story;
        }
    }
}
