﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace play
{
    public class AnimatedSprite
    {
        public Texture2D Texture { get; set; }
        public int Rows { get; set; }
        public int Columns { get; set; }
        private int _currentFrame;
        private readonly int _totalFrames;

        public AnimatedSprite(Texture2D texture, Vector2 frame)
        {
            Texture = texture;
            Rows = (int) frame.X;
            Columns = (int) frame.Y;
            _currentFrame = 0;
            _totalFrames = Rows * Columns;
        }
        public bool IsLastFrame
        {
            get
            {
                return _currentFrame == _totalFrames - 1;
            }
        }

        public void Update()
        {
            if(_currentFrame<_totalFrames)
                _currentFrame++;
            else if (_currentFrame >= _totalFrames)
                _currentFrame = 0;
        }

        public void Draw(SpriteBatch spriteBatch, Vector2 location)
        {
            int width = Texture.Width / Columns;
            int height = Texture.Height / Rows;
            var row = (int)((float)_currentFrame / (float)Columns);
            int column = _currentFrame % Columns;

            Rectangle sourceRectangle = new Rectangle(width * column, height * row, width, height);
            Rectangle destinationRectangle = new Rectangle((int)location.X, (int)location.Y, width, height);

            spriteBatch.Begin();
            spriteBatch.Draw(Texture, destinationRectangle, sourceRectangle, Color.White);
            spriteBatch.End();
        }
    }
}