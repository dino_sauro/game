﻿#region Using Statements

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

#endregion
namespace play
{
    class Area
    {

        //All Area Initializing
        private Vector4 _newArea;
        //public Area(int weight, int high, Vector2 position)
        public Area() { }
        public Area(int width, int heigh, Vector2 position)
        {
            _newArea = new Vector4(position.X, position.Y, position.Y + heigh, position.X + width);
        }
        public bool Limit(MouseState ms)
        {
            if (ms.X >= _newArea.X && ms.X <= _newArea.W && ms.Y >= _newArea.Y && ms.Y <= _newArea.Z)
            {
                return true;
            }
            return false;
        }

        public void Update(float xF)
        {
            _newArea.X-=xF;
            _newArea.W-=xF;
        }


    }
}
