﻿#region Using Statements
using System;
using System.Diagnostics;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

#endregion

namespace play
{

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        public SpriteBatch SBatch;
        private List<EnemyUnit> _enemyUnits;
        private int _xEunit;
        private List<AllyUnit> _allyUnits;
        private Castel _castel;
        private string _life;
        public static ContentManager ContentManager;
        readonly Random _nRandom = new Random();
        private int _nrand;

        private int _score;

        private MouseState _mouseState;
        //private bool _isFocus;
        //private Area _focus;
        private int _count;

        private bool _isClick;
        private int _countUpdate;

        //pause screen
        private bool _isPaused;
        public Texture2D PauseImage ;
        private Area _pause;
        public Vector2 PosPause;
        public Texture2D resumeImage;
        private static Area _resume;
        public Texture2D quitImage;
        private static Area _quit;

        public static Texture2D Soldat;
        public static Vector2 SoldatFrame;
        public static Texture2D Castle;
        public static Texture2D Dead;
        public static Vector2 DeadFrame;
        public static SpriteFont LineFont;
        public static Texture2D Knight;
        public static Vector2 KnightFrame;
        public static Texture2D Archer;
        public static Vector2 ArcherFrame;
        public static Texture2D Catapult;
        public static Vector2 CatapultFrame;
        public static Texture2D Lightning;
        public static Vector2 LightningFrame;

        public AnimatedSprite MouseAttack;

        private static Texture2D _gameOverScreen;
        private static Texture2D _tryagain;
        private static Area _tryAgainButton;
        public Game1()
        {

            new GraphicsDeviceManager(this)
                {
                    PreferredBackBufferHeight = 768,
                    PreferredBackBufferWidth = 1366,
                    IsFullScreen = true,

                    PreferMultiSampling = true,
                    SupportedOrientations = DisplayOrientation.LandscapeLeft | DisplayOrientation.LandscapeRight
                };

            Content.RootDirectory = "Content";
            ContentManager = Content;
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SBatch = new SpriteBatch(GraphicsDevice);
            //_focus = new Area(840, 86, new Vector2(300, 200));
            _tryAgainButton = new Area(363, 88, new Vector2(683, 384));

            _isClick = true;
            _countUpdate = 0;

            //Pause button and screen
            _isPaused = false;
            PauseImage = ContentManager.Load<Texture2D>("pauseBotton");
            PosPause = new Vector2(986, 100);
            _pause=new Area(38,37,PosPause);

            resumeImage = ContentManager.Load<Texture2D>("resumeBotton");
            _resume=new Area(205,101,new Vector2(500,300));

            quitImage = ContentManager.Load<Texture2D>("quitBotton");
            _quit=new Area(369,96,new Vector2(500,600));
            

            //Enemy and Castle load    
            Soldat = ContentManager.Load<Texture2D>("soldat");
            SoldatFrame = new Vector2(1, 9);
            Castle = ContentManager.Load<Texture2D>("castle2");
            Dead = ContentManager.Load<Texture2D>("dead");
            DeadFrame = new Vector2(8, 8);
            LineFont = ContentManager.Load<SpriteFont>("font");
            Knight = ContentManager.Load<Texture2D>("knigh");
            KnightFrame = new Vector2(1, 9);
            Catapult = ContentManager.Load<Texture2D>("catapult");
            CatapultFrame = new Vector2(1, 12);
            

            //Ally  and special attack load
            //Archer = ContentManager.Load<Texture2D>("archer");
            //ArcherFrame = new Vector2(1, 10);

            Lightning = ContentManager.Load<Texture2D>("lightning");
            LightningFrame = new Vector2(1, 8);
            MouseAttack = new AnimatedSprite(Lightning, LightningFrame);

            //Game Over Screen Load
            _gameOverScreen = ContentManager.Load<Texture2D>("gameover");
            _tryagain = ContentManager.Load<Texture2D>("tryagain");

            //TODO: use this.Content to load your game content here

           // _isFocus = true;

            _allyUnits = new List<AllyUnit>();
            _xEunit = 1;
            _enemyUnits = new List<EnemyUnit>();
            _castel = new Castel();
            _life = _castel.Life.ToString(CultureInfo.InvariantCulture);
            _score = 0;


        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            //TODO: Unload any non ContentManager content here
            _allyUnits.Clear();
            _enemyUnits.Clear();
            _allyUnits = null;
            _enemyUnits = null;
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (_pause.Limit(_mouseState))
                _isPaused = true;

            if (_isPaused)
            {
                if (_resume.Limit(_mouseState))
                    _isPaused = false;
                if(_quit.Limit(_mouseState))
                    Exit();
            }
            else
            {

                //TODO: Add your update logic here
                _mouseState = Mouse.GetState();


                // _isFocus = ( _mouseState.LeftButton == ButtonState.Pressed) && _focus.Limit(_mouseState);
                //if (_isFocus)
                //{
                //    _allyUnits.Add(new ArcherUnit());
                //    _isFocus = false;
                //}

                //foreach (AllyUnit t in _allyUnits)
                //{
                //    t.AnimatedSprite.Update();
                //}

                _count = _enemyUnits.Count;

                // remove dead soldiers
                _enemyUnits.RemoveAll(s => s.IsDead && s.AnimatedSprite.IsLastFrame);
                _score += _count - _enemyUnits.Count;

                foreach (EnemyUnit t in _enemyUnits)
                {
                    _mouseState = Mouse.GetState();
                    if (Math.Abs(t.Pos.X - _castel.Wall) > 0.01f)
                    {
                        t.Movement();
                        if (_mouseState.LeftButton == ButtonState.Released)
                        {
                            _isClick = false;
                            _countUpdate++;
                        }
                        if (_isClick && _countUpdate == 0)
                        {
                            t.Hitted(_mouseState);
                            _isClick = false;
                            _countUpdate++;
                        }
                            //else if(_countUpdate!=0 && !_isClick)
                            //{

                            //}
                        else
                        {
                            _countUpdate = 0;
                            _isClick = true;
                        }
                    }
                    else
                    {
                        _castel.Life = t.Damage(_castel.Life);
                        _life = _castel.Life.ToString(CultureInfo.InvariantCulture);
                        t.AnimatedSprite.Update();
                        t.Hitted(_mouseState);
                    }
                }

                if (_castel.IfLose())
                {
                    if ((_mouseState.LeftButton == ButtonState.Pressed) && _tryAgainButton.Limit(_mouseState))
                    {
                        Process.Start("play.exe");
                        Exit();
                    }

                }
                else
                {

                    _nrand = _nRandom.Next(0, 3);
                    switch (_nrand)
                    {
                        case 0:
                            _enemyUnits.Add(new EnemySoldat(_xEunit));
                            break;
                        case 1:
                            _enemyUnits.Add(new EnemyKnight(_xEunit));
                            break;
                        case 2:
                            _enemyUnits.Add(new EnemyCatapult(_xEunit));
                            break;
                    }
                    _xEunit += _nRandom.Next(10, 100);
                }

                MouseAttack.Update();

            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);



            if (_castel.IfLose())
            {
                SBatch.Begin();
                SBatch.Draw(_gameOverScreen, _castel.PosCastle, Color.White);
                SBatch.Draw(_tryagain, new Vector2(683, 384), Color.White);
                SBatch.End();
            }
            else
            {
                SBatch.Begin();
                SBatch.Draw(PauseImage,PosPause,Color.White);

                if (_isPaused)
                {
                    SBatch.Draw(resumeImage, new Vector2(500, 300), Color.White);
                    SBatch.Draw(quitImage,new Vector2(600,300),Color.White);
                }
                SBatch.Draw(_castel.Image, _castel.PosCastle, Color.White);


                SBatch.DrawString(LineFont, _life + "/1000", _castel.PosLife, Color.Black);
                SBatch.DrawString(LineFont, "Score:" + _score, new Vector2(50, 50), Color.Black);

                //_isFocus = _focus.Limit(_mouseState);
                //if (_isFocus)
                //{
                //    SBatch.Draw(Archer,new Vector2(200,200),Color.White);
                //    //SBatch.Draw(Archer, new Vector2(200, 200), Color.Black);
                //    //SBatch.DrawString(LineFont, "trollolol", new Vector2(200, 200), Color.Black);
                //}

                SBatch.End();


                foreach (EnemyUnit t in _enemyUnits)
                {
                    t.AnimatedSprite.Draw(SBatch, t.Pos);
                    if (_mouseState.LeftButton == ButtonState.Pressed && t.Area.Limit(_mouseState))
                    {
                        float temp;
                        temp = t.Pos.Y;
                        t.Pos.Y -= 470;
                        MouseAttack.Draw(SBatch, t.Pos);
                        t.Pos.Y = temp;
                    }
                }
                //foreach (AllyUnit t in _allyUnits)
                //{
                //    t.AnimatedSprite.Draw(SBatch, t.Pos);
                //}

                SBatch.End();
            }
            //TODO: Add your drawing code here
            base.Draw(gameTime);
        }
    }
}
