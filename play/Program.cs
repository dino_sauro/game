﻿#region Using Statements
using System;

#endregion

namespace play
{
    /// <summary>
    /// The main class.
    /// </summary>
    public static class Program
    {
        private static Game1 _game;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            _game = new Game1();
            _game.Run();
        }
    }
}
