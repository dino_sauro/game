﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace play
{
    public class AllyUnit
    {
        protected int NUnit = 0;
        protected float Attack;
        protected Texture2D Image;
        public AnimatedSprite AnimatedSprite;
        public Vector2 Pos;

        public AllyUnit(float attackDmg)
        {
            Attack = attackDmg;
        }

        public Vector2 LaunchAttack(int t)
        {
            //algoritmo per calcolare traiettoria arma
            Vector2 posArrow= new Vector2();

            posArrow.X = Pos.X*t;
            //posArrow.Y=Pos.Y*t+h

            return posArrow;


        }
        public void IncrementUnit()
        {
            NUnit++;
        }

        public float Damage()
        {
            return NUnit * Attack;
        }
    }
    public class ArcherUnit : AllyUnit
    {

        public ArcherUnit() : base(1.0f)
        {
            Pos= new Vector2(200,200);
            IncrementUnit();
            Image = Game1.Archer;
            AnimatedSprite = new AnimatedSprite(Image, Game1.ArcherFrame);
         }
    }
}
