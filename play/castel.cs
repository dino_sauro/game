﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace play
{
    class Castel
    {
        public Texture2D Image;
        public Vector2 PosCastle;
        public Vector2 PosLife;
        public float Life;
        public float Wall;

        public Castel()
        {
            PosCastle.X = 0;
            PosCastle.Y = 0;
            PosLife.X = 100;
            PosLife.Y = 450;
            Life = 1000;
            Image =Game1.Castle;
            Wall = 560;

        }
        public bool IfLose()
        {
            if (Life <= 0)
                return true;
            return false;
        }
    }
}
