﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using play;

namespace play
{
    class EnemyUnit
    {
        private int _life;
        protected float Attack;
        public Area Area;

        public int Life
        {
            get { return _life; }
            private set
            {
                _life = value;
                if (IsDead)
                {
                    Image = Game1.Dead;
                    Pos.Y = 600;
                    AnimatedSprite = new AnimatedSprite(Image, Game1.DeadFrame);
                }
            }
        }
        public Vector2 Pos;
        public Texture2D Image;
        public AnimatedSprite AnimatedSprite;


        public bool IsDead
        {
            get { return _life <= 0.0f; }
        }


        public EnemyUnit(float damage, int life)
        {
            Attack = damage;
            Life = life;
            Pos.X = 1360;
            Pos.Y = 675;

        }

        public virtual void Movement()
        {
            throw new System.NotImplementedException();
        }

        public int Damage(float castlelife)
        {
            return (int) (castlelife - Attack);
        }
        public void Hitted(MouseState mouseState)
        {
            if (!IsDead)
                if (Area.Limit(mouseState) && mouseState.LeftButton == ButtonState.Pressed)
                {
                    Life--;
                }
        }
    }

    class EnemySoldat : EnemyUnit
    {
        public EnemySoldat( int x)
            : base(0.1f, 10)
        {
            Image = Game1.Soldat;
            Pos.X += x;
            Area= new Area(64,71,Pos);
            AnimatedSprite = new AnimatedSprite(Image, Game1.SoldatFrame);

        }

        public override void Movement()
        {
            Pos.X--;
            AnimatedSprite.Update();
            Area.Update(1f);
        }
    }
    class EnemyKnight : EnemyUnit
    {
        public EnemyKnight(int x) : base(0.5f, 15)
        {
            Image = Game1.Knight;
            Pos.X += x;
            Pos.Y -= 70;
            Area = new Area(145,167, Pos);
            AnimatedSprite = new AnimatedSprite(Image, Game1.KnightFrame);
        }

        public override void Movement()
        {
            Pos.X-=0.5f;
            AnimatedSprite.Update();
            Area.Update(0.5f);
        }
    }
}
class EnemyCatapult : EnemyUnit
{
    public EnemyCatapult(int x) : base(1.0f,20)
    {
        Image = Game1.Catapult;
        Pos.X += x;
        Pos.Y -= 30;
        Area = new Area(128, 128, Pos);
        AnimatedSprite = new AnimatedSprite(Image, Game1.CatapultFrame);
    }
    override public void Movement()
    {
        Pos.X-=0.1f;
        AnimatedSprite.Update();
        Area.Update(0.1f);
    }
}
