﻿#region Using Statements
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Storage;
using Microsoft.Xna.Framework.GamerServices;
#endregion
namespace testgame
{
    class Area
    {

            //All Area Initializing
            Vector4 _newArea;
            //public Area(int weight, int high, Vector2 position)
            public Area() { }
            public Area(int weight,int high,Vector2 position)
            {
               _newArea = new Vector4(position.X, position.Y, position.Y+high, position.X+weight);
            }
        public bool Limit(MouseState ms)
        {
           if(ms.X>=_newArea.X && ms.X<=_newArea.W && ms.Y>=_newArea.Y && ms.Y<=_newArea.Z && ms.LeftButton== ButtonState.Pressed)
            {
                 return true;
            }
           return false;
        }


    }
}
