﻿#region Using Statements

using System.Drawing;
using System.Windows.Forms;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Color = Microsoft.Xna.Framework.Color;

#endregion

namespace testgame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        private AnimatedSprite animatedSprite;

       /* Vector2 mBAckGround = new Vector2(0, 0);
        Texture2D spriteBackground;

        Vector2 mPosition = new Vector2(300, 50);

        Texture2D quitSprite;
        Vector2 mQuit = new Vector2(300,200);

        Texture2D tutorialSprite;
        Vector2 mTutorial = new Vector2(300,300);

        Texture2D yesSprite;
        Vector2 mYes = new Vector2(100,300);

        Texture2D noSprite;
        Vector2 mNo = new Vector2(400, 300);

        Texture2D soundSprite;
        Vector2 mSound = new Vector2(700,30);*/

        // the spritesheet containing our animation frames
      /*  Texture2D spriteTitle;


        // the elapsed amount of time the frame has been shown for

        float time;
        // duration of time to show each frame
        float frameTime = 0.1f;
        // an index of the current frame being shown
        int frameIndex;
        // total number of frames in our spritesheet
        const int totalFrames =64;
        // define the size of our animation frame
        int frameHeight = 300;
        int frameWidth = 200;

        //music
       System.Media.SoundPlayer startSoundPlayer = new System.Media.SoundPlayer(@"C:\Users\X Vongola\Desktop\testgame\testgame\Content\music.wav");
        bool soundPLay;


        //mouse/button
        MouseState ms;
        Area test;
        Area quit;
        Area tutorial;
        Area yes;
        Area no;
        Area sound;*/
        private Cursor cursor;
        private System.Drawing.Graphics g;
        public Game1()
            : base()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //startSoundPlayer.PlayLooping();
            //soundPLay = true;
           cursor= new Cursor("Cursor1.cur");
            
        }


        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
           /* quit = new Area(151,82,mQuit);
            test = new Area(135,80,mPosition);
            tutorial = new Area(268,89,mTutorial);
            yes = new Area(132, 63, mYes);
            no = new Area(121, 59, mNo);
            sound = new Area(56, 72, mSound);*/

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Texture2D texture = Content.Load<Texture2D>("KnighWalkSheet_0");
            animatedSprite= new AnimatedSprite(texture,3,9);

            // TODO: use this.Content to load your game content here
          //  spriteBackground = this.Content.Load<Texture2D>("backGround");
            //spriteTitle = this.Content.Load<Texture2D>("boom3_0");
           /* quitSprite = this.Content.Load<Texture2D>("exit");
            tutorialSprite = this.Content.Load<Texture2D>("tutorial");
            yesSprite = this.Content.Load<Texture2D>("yes");
            noSprite = this.Content.Load<Texture2D>("no");
            soundSprite = this.Content.Load<Texture2D>("music");*/

         }
  
        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }
        
        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {

            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
             //   Exit();
            // TODO: Add your update logic here

            //Istruction that necessary for selection
           // ms = Mouse.GetState();
           // time += (float)gameTime.ElapsedGameTime.TotalSeconds;

            //Selection
          /*  if (quit.limit(ms))     //Button for quitting the game
            {
                /*spriteBackground = this.Content.Load<Texture2D>("quitChoicebackground");

                DrawQuit(gameTime);

                if(yes.limit(ms))
                    Exit();
                else if(no.limit(ms))
                    spriteBackground = this.Content.Load<Texture2D>("backGround");
                Exit();
                
            }
            if (tutorial.limit(ms))
            {
                spriteBackground = this.Content.Load<Texture2D>("tutorialImage");
            }
            else if (ms.LeftButton == ButtonState.Released)
            {
                spriteBackground = this.Content.Load<Texture2D>("background");
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Enter))
            {
                spriteTitle = this.Content.Load<Texture2D>("exit");

            }
            
            if (test.limit(ms))         //Button that start the play
            {

                spriteTitle = quitSprite;
                mPosition = mQuit;

            }
    
            if (sound.limit(ms))        //button that start/stop music
            {
                if (soundPLay == true)
                {
                    startSoundPlayer.Stop();
                    soundPLay = false;
                }
                else if(soundPLay == false)
                {
                    startSoundPlayer.PlayLooping();
                    soundPLay = true;
                }
            }*/

            // Process elapsed time
           
           /* while (time > frameTime)
            {
                // Play the next frame in the SpriteSheet
                frameIndex++;

                // reset elapsed time
                time = 0f;
            }
            if (frameIndex > totalFrames) frameIndex = 1;*/

            animatedSprite.Update();

            base.Update(gameTime);

            
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            GraphicsDevice.Clear(Color.CornflowerBlue);
            System.Drawing.Point p=new System.Drawing.Point();
            Size s = new Size(p);
            // TODO: Add your drawing code here
            System.Drawing.Rectangle r = new System.Drawing.Rectangle(p,s);
            cursor.Draw(g,r);
            animatedSprite.Draw(spriteBatch,new Vector2(300,100));
            /*spriteBatch.Begin();
            spriteBatch.Draw(spriteBackground, mBAckGround, Color.White);
            spriteBatch.End();*/
           

            //spriteBatch.Begin();
            //spriteBatch.Draw(button.Texture, mPosition, Color.AliceBlue);
           // spriteBatch.End();

            
            // Calculate the source rectangle of the current frame.
          /*  Rectangle source = new Rectangle(frameIndex * frameWidth, 0, frameWidth, frameHeight);
            // Calculate position and origin to draw in the center of the screen
            Vector2 position = new Vector2(this.Window.ClientBounds.Width / 2, this.Window.ClientBounds.Height / 2);
            Vector2 origin = new Vector2(frameWidth / 2.0f, frameHeight);
            

            // Draw the current frame.
            spriteBatch.Begin();
            spriteBatch.Draw(spriteTitle, position, source, Color.White, 0.0f,origin, 1.0f, SpriteEffects.None, 0.0f);
            spriteBatch.End();
            
            
            spriteBatch.Begin();
            spriteBatch.Draw(spriteTitle, mPosition, Color.Wheat);
            spriteBatch.End();
            
            spriteBatch.Begin();
            spriteBatch.Draw(quitSprite, mQuit, Color.Blue);
            spriteBatch.End();

            spriteBatch.Begin();
            spriteBatch.Draw(tutorialSprite, mTutorial, Color.Red);
            spriteBatch.End();

            spriteBatch.Begin();
            spriteBatch.Draw(soundSprite, mSound,Color.White);
            spriteBatch.End();*/

            base.Draw(gameTime);
        }


 /*       protected void DrawQuit(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            spriteBatch.Draw(spriteBackground, mBAckGround, Color.White);
            spriteBatch.End();

            spriteBatch.Begin();
            spriteBatch.Draw(yesSprite, mYes, Color.Coral);
            spriteBatch.End();

            spriteBatch.Begin();
            spriteBatch.Draw(noSprite, mNo, Color.Coral);
            spriteBatch.End();

            base.Draw(gameTime);
        }*/

    }
}
